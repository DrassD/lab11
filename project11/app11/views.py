from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse,JsonResponse
import requests
import json

def home(request):
	return render(request, 'home.html')

response = {}
url = "https://www.googleapis.com/books/v1/volumes?q=quilting"
def index(request):
    return render(request,"index.html")

def get_Google_book(request):
    raw_data = requests.get(url).json()

    data = [];
    for info in raw_data['items'] :
        item = {}
        item['id'] = info['id']
        item['title'] = info['volumeInfo']['title']
        item['authors'] = ", ".join(info['volumeInfo']['authors'])
        item['description'] = info['volumeInfo']['description'][:100]+"...."
        item['cover']=info['volumeInfo']['imageLinks']['thumbnail']
        data.append(item)

    return JsonResponse({"data" : data})